inzidenz = 18/100000
def chance_one_positive(tested, protection):
    chance = inzidenz
    # if tested reduce by 80%
    if tested:
        chance = chance / 5
    # reduce by protected value
    chance = chance * (1-protection)
    return chance
def chance_all_negative(number_of_people,tested,protection):
    return (1-chance_one_positive(tested,protection))**number_of_people
def chance(number_of_people,max_planck_chance,tested,protection):
    return (1-chance_all_negative(number_of_people,tested,protection)) * max_planck_chance
chance_infiziert_felix = chance(5,0.008,True,0.65)
print('Chance, dass ich infiziert bin',chance_infiziert_felix)
chance_ansteckung_felix = chance_infiziert_felix/6
print('Chance, dass ich jemanden infiziere',chance_ansteckung_felix)

chance_ansteckung_schule = chance(24,0.0091,True,0)
print('Chance, dass man sich an einem Tag in der Schule ansteckt',chance_ansteckung_schule)

chance_ansteckung_gesamt = chance_ansteckung_schule + chance_ansteckung_felix
print('Chance, dass ein Mensch, der erst einen Tag in die Schule geht und sich danach mit mir trifft erkrankt',chance_ansteckung_gesamt)
print('Chance, dass dieser Mensch wegen mir krank ist',chance_ansteckung_felix/chance_ansteckung_gesamt*100,'in Prozent')
