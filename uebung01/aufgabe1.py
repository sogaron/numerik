import math

def pq_formel(p,q):
    return [-1*p+math.sqrt(p**2+q),-1*p-math.sqrt(p**2+q)]

def pq_formel_alt(p,q):
    x = -1*p-math.sqrt(p**2+q)
    return [-1*q/x,x]

if __name__ == "__main__":
    q = 1
    ps = [10**2,10**4,10**6,10**7,10**8]
    for p in ps:
        print('p: '+str(p)+' q: '+str(q))
        print('Formel 1: '+str(pq_formel(p,1)))
        print('Formel 2: '+str(pq_formel_alt(p,1)))