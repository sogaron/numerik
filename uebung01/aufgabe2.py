import math

def e_func_1(x,n):
    e = 0
    for k in range(0,n+1):
        e += x**k/math.factorial(k)
    return e

def e_func_2(x,n):
    e = 0
    for k in range(0,n+1):
        e += x**(n-k)/math.factorial(n-k)
    return e

if __name__ == "__main__":
    tuples = [(1,1),(1,2),(1,3),(1,20),(5,1),(5,5)]
    for tuple in tuples:
        print('Tuple: '+str(tuple))
        print('Formel 1: '+str(e_func_1(tuple[0],tuple[1])))
        print('Formel 2: '+str(e_func_2(tuple[0],tuple[1])))