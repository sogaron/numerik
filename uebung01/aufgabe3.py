import math

def integrale_rect(f,a,b,n):
    h = (b-a)/n
    sum = 0
    for i in range(0,n):
        sum += f(a+i*h)
    return sum * h

def integrale_trap(f,a,b,n):
    h = (b-a)/n
    sum = 0
    for i in range(1,n):
        sum += f(a+i*h)
    ret = f(a) + f(b) + 2*sum
    ret *= h/2
    return ret

if __name__ == "__main__":
    f = lambda x : 1/(x*x)
    a = 1/10
    b = 10
    for n in range(1,102,10):
        print('n: '+str(n))
        print('Rechteck: '+str(integrale_rect(f,a,b,n)))
        print('Trapez: '+str(integrale_trap(f,a,b,n)))
    f = lambda x : math.log(x)
    a = 1
    b = 2
    for n in range(1,102,10):
        print('n: '+str(n))
        print('Rechteck: '+str(integrale_rect(f,a,b,n)))
        print('Trapez: '+str(integrale_trap(f,a,b,n)))
    