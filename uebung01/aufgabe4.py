import math
import numpy as np
import time
from datetime import datetime, date
def integrale_trap_vec(f,a,b,n):
    h = (b-a)/n
    help_func = lambda i : a+i*h
    ret = 2* np.sum(f(help_func(np.array(range(1,n))))) + f(a) +f(b)
    return  ret * h/2

def integrale_trap(f,a,b,n):
    h = (b-a)/n
    sum = 0
    for i in range(1,n):
        sum += f(a+i*h)
    ret = f(a) + f(b) + 2*sum
    ret *= h/2
    return ret

if __name__ == "__main__":
    f = lambda x : 1/(x*x)
    a = 1/10
    b = 10
    n = 10000000
    print('n: '+str(n))
    start = time.time()
    print('Trapez-Vektor: '+str(integrale_trap_vec(f,a,b,n)))
    vec_time = time.time() - start
    start = time.time()
    print('Trapez-Non-Vektor: '+str(integrale_trap(f,a,b,n)))
    norm_time = time.time() - start
    print('Vektor-Time: '+str(vec_time))
    print('Non-Vektor-Time: '+str(norm_time))