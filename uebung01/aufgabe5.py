import math
import numpy as np
import time
from datetime import datetime, date

def sqrt(x,x0,n):
    ak = math.sqrt(x0)
    yk = ak
    for k in range(1,n):
        ak *= (3/(2*k)-1)*(x/x0-1)
        yk += ak
    return yk

def sqrt_heron(x,x0,n):
    ak = math.sqrt(x0)
    yk = ak
    for k in range(1,n):
        ak *= (3/(2*k)-1)*(x/x0-1)
        yk = 1/2*(yk+x/yk)
    return yk

if __name__ == "__main__":
    # Heron ist immer schneller oder gleichschnell
    x = 2
    x0 = 100
    right_value = math.sqrt(x)
    printed_sqrt = False
    printed_heron = False
    delta = 0.005
    for n in range(1,1000000):
        sqrt_norm = sqrt(x,x0,n)
        heron = sqrt_heron(x,x0,n)
        if not(printed_sqrt) and abs(right_value - sqrt_norm) < abs(delta):
            print('sqrt unterschreitet delta bei n='+str(n))
            printed_sqrt = True
        if not(printed_heron) and abs(right_value - heron) < abs(delta):
            print('heron unterschreitet delta bei n='+str(n))
            printed_heron = True
        if printed_heron and printed_sqrt:
            break
    