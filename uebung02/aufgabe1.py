import math
import numpy as np
def zerlegung(A):
    pVektor= [0]* len(A)
    for i in range (len(A)) :
        #vertauschen
        if A[i][i] == 0 :
            for j in range (i, len(A)) :
                if A[j][i] != 0 :
                    pVektor[i] = j
                    A[i], A[j] = A[j], A[i]
                    break
        else :
            pVektor[i] = i 
            #Ende vertauschen
        #quotienten ermitteln und eintragen
        for j in range (i+1, len(A)) :
            A[j][i] /= A[i][i]
            for k in range (i+1, len(A)) :
                A[j][k]-=A[j][i]*A[i][k] 
    return {"LU":A,"p":pVektor}

def swap(list,indexA,indexB):
    temp = list[indexA]
    list[indexA]= list[indexB]
    list[indexB] = temp

def permutation(p,x):
    y = x + []
    for i in range(0,len(p)):
        swap(y,p[i],i)
    return y


def vorwaerts(LU,x):
    w = int(math.sqrt(len(LU)))
    y = []
    for i in range(0,len(LU),w+1):
        y_temp = x[i%w]
        temp = 0
        for j in range(0,i%w):
            temp += y[j] * LU[int(i/w)*w+j]
        y_temp -= temp
        y.append(y_temp)
    return y

def rueckwaerts(LU,x):
    w = int(math.sqrt(len(LU)))
    y = []
    for i in range(len(LU)-1,-1,-w-1):
        y_temp = x[i%w]
        temp = 0
        for j in range(i%w,w-1):
            temp += y[w-2-j] * LU[int(i/w)*w+j+1]
        y_temp -= temp
        y.append(y_temp/LU[i])
    return list(reversed(y))

if __name__ == "__main__":
    A = [[0,0,0,1],[2,1,2,0],[4,4,0,0],[2,3,1,0]]
    zerlegt = zerlegung(A)
    LU = []
    for array in zerlegt['LU']:
        LU += array
    p = zerlegt['p']
    b = [3,5,4,5]
    b2 = [4,10,12,11]
    y = vorwaerts(LU,permutation(p,b))
    print(y)
    print(rueckwaerts(LU,y))