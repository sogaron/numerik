import math
import numpy

def matrixAnlegen(n):
    L = numpy.zeros((n,n))
    for i in range (0,n):
        L[i][i] = 2
        if (i-1>=0) :
            L[i][i-1]= -1
        if (i+1<n) :
            L[i][i+1]= -1
    return L

def choleskyZerlegung(L):
    for i in range (0,len(L)):
        for j in range(i,len(L)):
            if i==j:
                zwischenergebnis=0
                for k in range (0,i):
                    zwischenergebnis+= L[k][j]*L[k][j]
                L[i][j] = math.sqrt(L[i][j]-zwischenergebnis)
            else :
                zwischenergebnis=0
                for k in range (0,i):
                    zwischenergebnis+= L[k][j]*L[k][i]
                L[i][j] = (L[i][j]-zwischenergebnis)/L[i][i]
                L[j][i] = L[i][j]
    return L

print(choleskyZerlegung(matrixAnlegen(100)))
print(choleskyZerlegung(matrixAnlegen(1000)))
print(choleskyZerlegung(matrixAnlegen(10000)))
            