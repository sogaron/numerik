import math
import numpy as np
def zerlegung(A):
    LU = np.copy(A).tolist()
    pVektor= [0]* len(LU)
    for i in range (len(LU)) :
        #vertauschen
        if LU[i][i] == 0 :
            for j in range (i, len(LU)) :
                if LU[j][i] != 0 :
                    pVektor[i] = j
                    LU[i], LU[j] = LU[j], LU[i]
                    break
        else :
            pVektor[i] = i 
            #Ende vertauschen
        #quotienten ermitteln und eintragen
        for j in range (i+1, len(LU)) :
            LU[j][i] /= LU[i][i]
            for k in range (i+1, len(LU)) :
                LU[j][k]-=LU[j][i]*LU[i][k] 
    return (LU,pVektor)

def permutation(p,x):
    y = np.copy(x)
    for i in range(0,len(p)):
        y[p[i]],y[i] = y[i],y[p[i]]
    return y

def permutation_backwards(p,x):
    y = np.copy(x)
    for i in range(len(p)-1,-1,-1):
        y[p[i]] , y[i] = y[i] , y[p[i]]
    return y


def vorwaerts(LU,x):
    y = np.zeros((len(LU)))
    for i in range(0,len(LU)):
        y_temp = x[i]
        temp = 0
        for j in range(0,i):
            temp += y[j] * LU[i][j]
        y_temp -= temp
        y[i] = y_temp
    return y

def rueckwaerts(LU,x):
    y = np.zeros((len(LU)))
    for i in range(len(LU)-1,0,-1):
        y_temp = x[i]
        temp = 0
        for j in range(i+1,len(LU)):
            temp += y[j] * LU[i][j]
        y_temp /= LU[i][i]
        y[i] = y_temp
    return y

def get_P_from_p(p):
    P = np.zeros((len(p),len(p)))
    for i in range(0,len(p)):
        P[i][i] = 1
    for i in range(0,len(p)):
        P[[i,p[i]]] = P[[p[i],i]]
    return P

def get_v(LU,c):
    v = np.zeros((len(c)))
    for i in range(0,len(LU)):
        v_temp = c[i]
        temp = 0
        for j in range(0,i):
            temp +=v[j] * LU[i][j]
        v_temp -= temp
        v_temp /= LU[i][i]
        v[i] = v_temp
    return v

def get_w(LU,v):
    w = np.zeros((len(LU)))
    for i in range(len(LU)-1,-1,-1):
        w_temp = v[i]
        temp = 0
        for j in range(i+1,len(LU)):
            temp += w[j] * LU[i][j]
        w_temp -= temp
        w[i] = w_temp
    return w
    

def solve_transposed_from(LU,p,c):
    LU_T = np.transpose(np.array(LU))
    v = get_v(LU_T,c)
    w = get_w(LU_T,v)
    return permutation_backwards(p,w)

def solve(A,b):
    LU,p = zerlegung(A)
    y = vorwaerts(LU,permutation(p,b))
    x = rueckwaerts(LU,y)
    return (x,LU,p)

if __name__ == "__main__":
    np.seterr('raise')
    A = np.array([[0,0,0,1],[2,1,2,0],[4,4,0,0],[2,3,1,0]])
    b = np.array([3,5,4,5])
    x,LU,p = solve(A,b)
    print('x',x)
    c = np.array([152,154,56,17])
    z = solve_transposed_from(LU,p,c)
    print('z',z)