import numpy.linalg as linalg
import math
import numpy as np
import aufgabe1 as lu

def sign(vector):
    sign = np.zeros((len(vector)))
    for i in range(0,len(vector)):
        if vector[i] > 0:
            sign[i] = 1
        elif vector[i] == 0:
            sign[i] = 0
        else:
            sign[i] = 1
    return sign

def get_biggest(vec):
    index = 0
    biggest = vec[0]
    for i in range(1,len(vec)):
        if abs(vec[i]) > biggest:
            biggest = abs(vec[i])
            index = i
    return (biggest,index)

def sum_rows_abs(A):
    sum_rows = np.zeros((len(A)))
    for i in range(0,len(A)):
        sum_rows[i] = sum_vec_abs(A[i])
    return sum_rows

def sum_vec_abs(vec):
    sum = 0
    for cell in vec:
        sum += abs(cell)
    return sum

def get_condition(A,LU,p):
    x = np.ones((len(A))) / len(A)
    y = None
    iterations = 0
    while True:
        iterations += 1
        # y = lu.solve_transposed_from(LU,p,x)
        y = np.transpose(A).dot(x)
        eps = sign(y)
        z = A.dot(eps)
        biggest, j = get_biggest(z)
        if biggest <= np.transpose(z).dot(x):
            break
        x = np.zeros((len(A)))
        x[j] = 1
    print('y',y)
    summ_norm_inv = sum_vec_abs(y)
    print('summ_norm_inv',summ_norm_inv)
    summ_norm = get_biggest(sum_rows_abs(A))[0]
    print('summ_norm',summ_norm)
    return summ_norm*summ_norm_inv

if __name__ == "__main__":
    deltas = [10**-8]#,10**-10,10**-12]
    for delta in deltas:
        A = np.array([[3,2,1],[2,2*delta,2*delta],[1,2*delta,-1*delta]])
        summ_norm = get_biggest(sum_rows_abs(A))[0]
        summ_norm_inv = get_biggest(sum_rows_abs(linalg.inv(A)))[0]
        print('cond',summ_norm*summ_norm_inv)
        LU,p = lu.zerlegung(A)
        print(get_condition(A,LU,p))
        print('cond',linalg.cond(A,np.inf))