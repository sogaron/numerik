import numpy as np
import math
def solve_LU_pk_is_rk(LU,r_k,p):
    return rueckwaerts(LU,vorwaerts(LU,permutation(p,r_k)))

def nachschritt(A,LU,p,x,b):
    LU = np.array(LU)
    x_k = x.copy()
    b = np.array(b)
    while True:
        r_k = np.subtract(b,np.dot(A,x_k))
        p_k = solve_LU_pk_is_rk(LU,r_k,p)
        x_k += p_k
        if np.linalg.norm(p_k)/np.linalg.norm(x_k) < 0.1:
            break
    return x_k



def zerlegung(AA):
    A = AA.copy()
    pVektor= [0]* len(A)
    for i in range (len(A)) :
        #vertauschen
        biggest = A[i][i]
        index = i
        for j in range(i,len(A)):
            if abs(A[j][i]) > biggest:
                biggest = abs(A[j][i])
                index = j
        pVektor[i] = index
        A[index], A[i] = A[i], A[index]
        #quotienten ermitteln und eintragen
        for j in range (i+1, len(A)) :
            A[j][i] /= A[i][i]
            for k in range (i+1, len(A)) :
                A[j][k]-=A[j][i]*A[i][k] 
    return (A,pVektor)

def swap(list,indexA,indexB):
    temp = list[indexA]
    list[indexA]= list[indexB]
    list[indexB] = temp

def permutation(p,x):
    y = np.copy(x)
    for i in range(0,len(p)):
        y[p[i]],y[i] = y[i],y[p[i]]
    return y



def vorwaerts(LU,x):
    y = np.zeros((len(LU)))
    for i in range(0,len(LU)):
        y_temp = x[i]
        temp = 0
        for j in range(0,i):
            temp += y[j] * LU[i][j]
        y_temp -= temp
        y[i] = y_temp
    return y

def rueckwaerts(LU,x):
    y = np.zeros((len(LU)))
    for i in range(len(LU)-1,-1,-1):
        y_temp = x[i]
        temp = 0
        for j in range(i+1,len(LU)):
            temp += y[j] * LU[i][j]
        y_temp -= temp
        y_temp /= LU[i][i]
        y[i] = y_temp
    return y

def solve(A,b):
    LU,p = zerlegung(A)
    y = vorwaerts(LU,permutation(p,b))
    x = rueckwaerts(LU,y)
    return nachschritt(A,LU,p,x,b)

def get_b(n):
    b = np.zeros((n))
    b[0] = 2
    b[1] = 1
    b[-1] = 2 - n
    b[-2] = 4 - n
    return b

def get_A(n):
    A = np.zeros((n,n))
    i = 0
    for row in A:
        j = 0
        for ele in row:
            if j == n-1 or i == j:
                row[j]= 1
            elif j<i:
                row[j] = -1
            j += 1
        i +=1
    return A
if __name__ == "__main__":
    ns = [9]
    for n in ns:
        A = get_A(n)
        b = get_b(n)
        x = solve(A,b)
        print('A',A)
        print('b',b)
        print('x',x)
        print('Ax',A.dot(x.T))