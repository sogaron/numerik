import Blatt06_lib
import numpy as np


def CGVerfahren(A,b):
    x0 = np.ones(len(b))
    r1=A.dot(x0)-b
    d1=-r1
    x1=x0
    while np.linalg.norm(r1)>1e-6:
        r=r1
        d=d1
        x=x1
        Ap=A.dot(d)
        alpha=np.linalg.norm(r)**2/(np.dot(Ap,d))
        x1=x+alpha*d
        r1=r+alpha*Ap
        beta=np.linalg.norm(r1)**2/(np.linalg.norm(r)**2)
        d1=-r1+beta*d
    return x1


if __name__ == "__main__":
    for m in [50, 100, 200]:
        A, b = Blatt06_lib.system(m)
        x = CGVerfahren(A, b)
        print (x)
        Blatt06_lib.plotxk(x)
        
