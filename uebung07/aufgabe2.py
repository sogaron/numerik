import numpy as np 
def get_mü(A):
	n = len(A)-1
	B = np.array([[A[n-1][n-1],A[n-1][n]],[A[n][n-1],A[n][n]]])
	eigenwerte,eigenvektoren = np.linalg.eig(B)
	min = eigenwerte[0]
	for eigenwert in eigenwerte:
		if abs(eigenwert-A[n][n]) < abs(min-A[n][n]):
			min = eigenwert
	return min
def qr_iteration(A,eps):
	A_k = np.copy(A)
	Q_k = np.identity(len(A_k))
	n = len(A_k)-1
	mü_k = None
	while n != 0:
		while abs(A_k[n][n-1]) > eps:
			mü_k = get_mü(A)
			q_k,r_k = np.linalg.qr(A_k - mü_k*np.identity(len(A_k)))
			A_k = np.add(r_k.dot(q_k),mü_k*np.identity(len(A_k)))
			Q_k = Q_k.dot(q_k)
		n -= 1
	eigenwerte = np.zeros(len(A_k))
	for i in range(0,len(A_k)):
		eigenwerte[i] = A_k[i][i]
	eigenvektoren = Q_k
	return (eigenwerte,eigenvektoren)

def get_A(m):
	A = np.zeros((m**2,m**2))
	for i in range(0,m):
		for j in range(0,m):
			if i == j:
				A[i][j] = 4
			if abs(i-j) == 1:
				A[i][j] = -1
			if abs(i-j) == m:
				A[i][j] = 1
	return A
		
def main():
	m = 10
	eps = 10**-8
	A = get_A(m)
	eigenwerte,eigenvektoren = qr_iteration(A,eps)
	print('eigenwerte',eigenwerte)
	print('eigenvektoren',eigenvektoren)

if __name__ == "__main__":
	main()

