import math
import numpy as np
import math as m

def newtonVerfahren():
    count = 0
    xk = 2000 
    while m.fabs(b(xk)) > 1e-7:
        if b1(xk) ==0:
            break
        xk = xk - (b(xk)/b1(xk))
        count +=1
    print("Newtonverfahren count:"+str(count))
    return xk

def b(x):
    return (9.8606 / (1 +1.1085e25*np.exp(-0.029*x)))


def b1(x):
    a = 9.8606
    c = -1.1085e25
    d = 0.029
    zaehler = a*c*d*np.exp(d*x)
    nenner = (np.exp(d*x)-c)**2
    return -zaehler/nenner

def sekantenVerfahren():
    count =0
    xk = 2000
    xk1 = 1961
    while m.fabs(b(xk)) > 1e-7:
        if b(xk) - b(xk1)==0:
            break
        xkneu = xk - ((xk-xk1) / (b(xk) - b(xk1) )) * b(xk)
        xk1 = xk
        xk= xkneu
        count+=1
    print("Sekantenverfahren count:"+str(count))
    return xk

if __name__ == "__main__":
    print (newtonVerfahren())
    print(sekantenVerfahren())
