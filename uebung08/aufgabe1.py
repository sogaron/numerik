import numpy as np
def newton_unopt(func,derivative,x0,iterations):
    x_n = x0
    ret = [x0]
    for _ in range(0,iterations):
        x_n = x_n - func(x_n)/derivative(x_n)
        ret.append(x_n)
    return ret

def newton_var1(f,f_dx,x0,iterations,q):
    x_n = x0
    ret = [x0]
    for _ in range(0,iterations):
        x_n = x_n - q*f(x_n)/f_dx(x_n)
        ret.append(x_n)
    return ret

def newton_var2(f,f_dx,f_dx2,x0,iterations,q):
    x_n = x0
    ret = [x0]
    for _ in range(0,iterations):
        x_n = x_n - f(x_n)*f_dx(x_n)/(f_dx(x_n)**2-f(x_n)*f_dx2(x_n))
        ret.append(x_n)
    return ret


if __name__ == "__main__":
    f = lambda x : np.arctan(x)-x
    f_dx = lambda x : 1/(x**2+1)-1
    f_dx2 = lambda x : -2*x/(x**2+1)**2
    x0 = 1
    q = 3
    iterations = 4
    print('Standard',newton_unopt(f,f_dx,x0,iterations))
    print('Variante 1',newton_var1(f,f_dx,x0,iterations,q))
    print('Variante 2',newton_var2(f,f_dx,f_dx2,x0,iterations,q))