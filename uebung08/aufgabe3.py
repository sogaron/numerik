import numpy as np
import math
def newton_dim2(f,J,x0,y0,iterations):
    x_n = x0
    y_n = y0
    for _ in range(0,iterations):
        x_n_delta = np.linalg.inv(np.array(J(x_n,y_n))).dot(np.array(f(x_n,y_n)))
        x_n, y_n = tuple(np.subtract(x_n,x_n_delta))
    return (x_n,y_n)

if __name__ == "__main__":
    e = math.e
    f = lambda x,y : (math.sin(x)-y,e**(-y)-x)
    J = lambda x,y : ((math.cos(x),-1),(-1,-e**(-y)))
    x0 = 1
    y0 = 1
    iterations = 100000
    x,y = newton_dim2(f,J,x0,y0,iterations)
    print('x',x)
    print('y',y)
    print('f(x,y)',f(x,y))