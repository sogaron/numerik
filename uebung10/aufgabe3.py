import numpy as np
import matplotlib.pyplot as plt

def nat_cub_spline(points):
    h_i = []
    for i in range(len(points)-1):
        h_i.append(points[i+1][0] - points[i][0])
    gamma_i = []
    for i in range(1,len(points)-1):
        gamma_i.append( 6 * ( (points[i+1][1] - points[i][1]) / h_i[i] - ( points[i][1] - points[i-1][1] ) / h_i[i-1] ) )
    A = np.zeros((len(points)-2,len(points)-2))
    for i in range(len(points)-2):
        for j in range(len(points)-2):
            if i == j:
                A[i,j] = 2 * ( h_i[i]+h_i[i+1])
            elif i-j == 1:
                A[i,j] = h_i[i]
            elif i-j == -1:
                A[i,j] = h_i[j]
    beta_i = np.linalg.solve(A,np.array(gamma_i))
    beta_i = np.insert(beta_i,0,0)
    beta_i = np.insert(beta_i,len(beta_i),0)
    alpha_i = []
    for i in range(len(beta_i)-1):
        alpha_i.append( ((points[i+1][1]-points[i][1])/h_i[i] ) - 1/3*beta_i[i]*h_i[i]- 1/6*beta_i[i+1]*h_i[i] )
    polynoms = []
    for i in range(len(points)-1):
        polynoms.append(lambda x : points[i][1] + alpha_i[i]*(x-points[i][0]) + beta_i[i]/2 * ( x - points[i][0] )**2 + (beta_i[i+1]-beta_i[i])/(6*h_i[i]) * (x-points[i][0])**3)
    return polynoms


if __name__ == "__main__":
    # 3 a)
    x_i = [-3,-1,0,1,3]
    f = lambda x : 1/(1+x**2)
    points = []
    for x in x_i:
        points.append((x,f(x)))
    f = nat_cub_spline(points)
    print('f(-3)',f[0](-3))
    print('f(-1)',f[0](-1))
    print('f(-1)',f[1](-1))
    print('f(0)',f[1](0))
    print('f(0)',f[2](0))
    print('f(1)',f[2](1))
    print('f(1)',f[3](1))
    print('f(3)',f[3](3))
    print('points',points)
    # 3 b)
    ms = [7,9,11]
    f = lambda x : 0.5 + 1/(1+x**2)
    x = np.linspace(-5,5,100)
    for m in ms:
        points = []
        for i in range(m):
            x_i = -5 + 10/(m-1)*i
            points.append((x_i,f(x_i)))
        pol = nat_cub_spline(points)
        print('f(5)',pol[len(pol)-1](5))
        # plt.plot(x,pol[0](x),'r')
        # plt.plot(x,pol[1](x),'r')
        # plt.plot(x,pol[2](x),'r')
        # plt.plot(x,pol[3](x),'r')
        # plt.show()
    # 3 c)
    ms = [7,9,11]
    f = lambda x : 0.5 + 1/(1+x**2)
    x = np.linspace(-5,5,100)
    for m in ms:
        points = []
        for i in range(m):
            x_i = -5 * np.cos(np.pi*(2*i+1)/(2*m))
            points.append((x_i,f(x_i)))
        pol = nat_cub_spline(points)
        print('f(5)',pol[len(pol)-1](5))
        # plt.plot(x,pol[0](x),'r')
        # plt.plot(x,pol[1](x),'r')
        # plt.plot(x,pol[2](x),'r')
        # plt.plot(x,pol[3](x),'r')
        # plt.show()
    
    # points = [(-3,3/5),(-1,1),(0,3/2),(1,1),(3,3/5)]
    # f = nat_cub_spline(points)

    