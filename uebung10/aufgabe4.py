import numpy as np
import matplotlib.pyplot as plt
import decimal
import numbers
def nat_cub_spline(points):
    h_i = []
    for i in range(len(points)-1):
        h_i.append(points[i+1][0] - points[i][0])
    gamma_i = []
    for i in range(1,len(points)-1):
        gamma_i.append( 6 * ( (points[i+1][1] - points[i][1]) / h_i[i] - ( points[i][1] - points[i-1][1] ) / h_i[i-1] ) )
    A = np.zeros((len(points)-2,len(points)-2))
    for i in range(len(points)-2):
        for j in range(len(points)-2):
            if i == j:
                A[i,j] = 2 * ( h_i[i]+h_i[i+1])
            elif i-j == 1:
                A[i,j] = h_i[i]
            elif i-j == -1:
                A[i,j] = h_i[j]
    beta_i = np.linalg.solve(A,np.array(gamma_i))
    beta_i = np.insert(beta_i,0,0)
    beta_i = np.insert(beta_i,len(beta_i),0)
    alpha_i = []
    for i in range(len(beta_i)-1):
        alpha_i.append( ((points[i+1][1]-points[i][1])/h_i[i] ) - 1/3*beta_i[i]*h_i[i]- 1/6*beta_i[i+1]*h_i[i] )
    ret = Hilfsklasse(points,alpha_i,beta_i,h_i)
    return lambda x : ret.get_val(x)

class Hilfsklasse:
    
    def __init__(self,points,alpha_i,beta_i,h_i):
        self.points = points
        self.alpha_i = alpha_i
        self.beta_i = beta_i
        self.h_i = h_i

    def get_val(self,x):
        if(not(isinstance(x,numbers.Number))):
            ret = []
            for x_i in x:
                ret.append(self.get_val(x_i))
            return ret
        for i in range(len(self.points)):
            if self.points[i][0] > x:
                return self.points[i][1] + self.alpha_i[i]*(x-self.points[i][0]) + self.beta_i[i]/2 * ( x - self.points[i][0] )**2 + (self.beta_i[i+1]-self.beta_i[i])/(6*self.h_i[i]) * (x-self.points[i][0])**3
    


if __name__ == "__main__":
    ms = [6,7,8]
    x = lambda x : np.cos(3*np.pi*x) / ( 1 + x )
    y = lambda x : np.sin(3*np.pi*x) / ( 1 + x )
    for m in ms:
        points_x = []
        points_y = []
        for i in range(m):
            t_i = i/(m-1)
            points_x.append((t_i,x(t_i)))
            points_y.append((t_i,y(t_i)))
        s_x = nat_cub_spline(points_x)
        s_y = nat_cub_spline(points_y)
        x_lin = np.linspace(0.1,0.7,50)
        plt.plot(x_lin,s_y(x_lin),'b')    
    plt.plot(x_lin,y(x_lin),'r')
plt.show()
    