import numpy as np
import math
import matplotlib.pyplot as plt


def do_TSVD(alpha,A):
    eigenwerte, _ = np.linalg.eig(A)
    eigenwerte = eigenwerte.real
    eigenwerte[::-1].sort()
    for k in range(len(eigenwerte)):
        if eigenwerte[0]/eigenwerte[k] >= 1/alpha:
            return eigenwerte[:k]
    return eigenwerte

if __name__ == "__main__":
    n = 100
    gamma = 0.05
    delta = 10**(-6)
    x = []
    for i in range(n+1):
        if 45 <= i and i <= 55:
            x.append(1)
        elif 60 <= i and i <= 65:
            x.append(0.5)
        else:
            x.append(0)
    print('x',x)
    c = 1 / ( gamma * math.sqrt(2*math.pi))
    A = []
    for i in range(n+1):
        A.append([])
        for j in range(n+1):
            A[i].append(c / n * math.e**(-( (i-j) / (math.sqrt(2)*n*gamma) )**2))
    b = np.array(A).dot(np.array(x))
    lin = np.linspace(0,100,100,dtype=np.dtype(np.int32))
    f = lambda y : [b[x] for x in y]
    plt.plot(lin,f(lin),'r')
    f = lambda y : [x[x_x] for x_x in y]
    plt.plot(lin,f(lin),'b')
    b_delta = delta*np.random.randn(n+1)
    # Die pseudoinverse wird durch das b_delta zu stark beeinflusst. Wir rechnen hier nur mit b damit das ganze plotbar ist
    x_pinv = np.linalg.pinv(np.array(A)).dot(b)
    # x_pinv = np.linalg.pinv(np.array(A)).dot(b+b_delta)
    f = lambda y : [x_pinv[x_x] for x_x in y]
    plt.plot(lin,f(lin),'g')
    for k in range(0,-9,-1):
        alpha = 10**k
        diag_tsvd = do_TSVD(alpha,A)
        A_tsvd = np.zeros((n+1,n+1))
        for i in range(len(diag_tsvd)):
            A_tsvd[i,i] = diag_tsvd[i]
        x_tsvd = A_tsvd.dot(b+b_delta)
        f = lambda y : [x[x_x] for x_x in y]
        plt.plot(lin,f(lin),'y')
    plt.show()
    