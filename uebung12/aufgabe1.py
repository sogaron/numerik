import math
import numpy as np

def trapez_regel(f,a,b,m):
    trapez = f(a)+f(b)
    for i in range(1,m):
        trapez += 2*f(i/m)
    trapez /= 2
    trapez *= (b-a) / m
    return trapez
   
def simpson_regel(f,a,b,m):
    simpson = 0
    l = (b-a) / m
    for i in range(0,m+1):
        factor = None
        if i == 0 or i==m:
            factor = 1
        elif i % 2 == 1:
            factor = 4
        else:
            factor = 2
        a_i = a + i*l
        simpson += factor* f(a_i)
    simpson *= l/3
    return simpson

if __name__ == "__main__":
    # 1 a)
    print('1 a)')
    exact = math.pi / 4
    print('exakte Loesung',exact)
    # 1 b)
    print('')
    print('1 b)')
    f = lambda x : 1/(1+x**2)
    trapez = trapez_regel(f,0,1,8)
    print('Loesung mit summierter Trapez-Regel',trapez)
    print('absoluter Fehler',abs(exact-trapez))
    # 1 c)
    print('')
    print('1 c)')
    simpson = simpson_regel(f,0,1,4)
    print('Loesung mit summierter Simpson-Regel',simpson)
    print('absoluter Fehler',abs(exact-simpson))
    # 1 d)
    print('')
    print('1 d)')
    x = [-1*math.sqrt(3/5),0,math.sqrt(3/5)]
    beta = [5/9,8/9,5/9]
    gauss = 0
    for i in range(len(x)):
        gauss += beta[i] * f(x[i])
    print('Loesung mit Gauss-Verfahren',gauss)
    print('absoluter Fehler',abs(exact-gauss))
    