import math
import numpy as np
from aufgabe1 import trapez_regel
import scipy.integrate as integrale

def romberg_folge(ord,a,b):
    l = [b-a]
    for i in range(1,ord//2+1):
        l.append(l[i-1]/2)
    return l

def bulirsch_folge(ord,a,b):
    l = [b-a,(b-a)/2,(b-a)/3]
    for i in range(3,ord//2+1):
        l.append(l[i-2]/2)
    if ord == 2:
        return l[:1]
    elif ord == 4:
        return l[:2]
    return l

def extrapolation(f,a,b,l):
    P = np.zeros((len(l),len(l)))
    for i,ele in enumerate(l):
        P[i,0] = trapez_regel(f,a,b,int((b-a)/ele))
    for i in range(1,len(l)):
        for j in range(1,i+1):
            P[i,j] = P[i,j-1] + (P[i,j-1]-P[i-1,j-1]) / ( ( l[i-j] / l[i] )**2 - 1 )
    return P[len(l)-1,len(l)-1]

if __name__ == '__main__':
    format = lambda x : "{:1.15}".format(x)
    f = lambda x : math.sin(math.pi * x**2)
    a = -1
    b = 1
    exact,_ =  integrale.quad(f,a,b)
    # 2 a)
    print('2 a/b)')
    print('exact',exact)
    i_string = " i "
    romberg_string = " R "
    for i in range(2,18,2):
        romberg = romberg_folge(i,a,b)
        i_string += "         "+str(i)+"        "
        romberg_string += " "+format(extrapolation(f,a,b,romberg))+" "
    # 2 b)
    bulirsch_string = " B "
    for i in range(2,18,2):
        bulirsch = bulirsch_folge(i,a,b)
        bulirsch_string += " "+format(extrapolation(f,a,b,bulirsch))+" "
    print(i_string)
    print(romberg_string)
    print(bulirsch_string)