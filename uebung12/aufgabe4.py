import math
import numpy as np
def mult_tuple(tuple,factor):
    return (tuple[0] * factor,tuple[1] * factor)


def steepest_descent(f,f_dx,start,alpha = 1,p=0.5,tau=0.5,eps=10**-2):
    x_k = start
    p_k = mult_tuple(f_dx(x_k),-1)
    alpha_k = alpha
    phi = lambda a : f((x_k[0]+mult_tuple(p_k,a)[0],x_k[1]+mult_tuple(p_k,a)[1]))
    phi_dx_0 = lambda : f_dx(x_k)[0]*p_k[0] + f_dx(x_k)[1]*p_k[1]
    lam = lambda a : phi(0) + p*phi_dx_0()*a
    while np.linalg.norm(f_dx(x_k),np.inf) > eps:
        p_k = mult_tuple(f_dx(x_k),-1)
        alpha_temp = alpha_k
        while True:
            if phi(alpha_temp) <= lam(alpha_temp):
                alpha_k = alpha_temp
                break
            else:
                alpha_temp *= tau
        x_k = (x_k[0] + mult_tuple(p_k,alpha_k)[0],x_k[1] + mult_tuple(p_k,alpha_k)[1])
    return x_k

if __name__ == "__main__":
    f = lambda xy: ( math.sin(xy[0]) - xy[1] )**2 + ( math.e**( -1*xy[1] ) - xy[0] )**2
    f_dx1 = lambda x,y : 2*math.cos(x)*(math.sin(x) - y) - 2*(math.e**(-1*y)-x)
    f_dx2 = lambda x,y : -2*math.e**(-1*y)*(math.e**(-1*y)-x) - 2*(math.sin(x) - y)
    f_dx = lambda xy : (f_dx1(xy[0],xy[1]),f_dx2(xy[0],xy[1]))
    starts = [(5,2),(6,2),(-1,-1),(-2,-2)]
    for start in starts:
        print('start',start)
        print(steepest_descent(f,f_dx,start))