import math
import numpy as np
def zerlegung(A):
    pVektor= [0]* len(A)
    for i in range (len(A)) :
        #vertauschen
        biggest = A[i][i]
        index = i
        for j in range(i,len(A)):
            if abs(A[j][i]) > biggest:
                biggest = abs(A[j][i])
                index = j
        pVektor[i] = index
        A[index], A[i] = A[i], A[index]
        #quotienten ermitteln und eintragen
        for j in range (i+1, len(A)) :
            A[j][i] /= A[i][i]
            for k in range (i+1, len(A)) :
                A[j][k]-=A[j][i]*A[i][k] 
    return {"LU":A,"p":pVektor}

def zerlegung_ohne_pivot(A):
    pVektor= [0]* len(A)
    for i in range (len(A)) :
        #vertauschen
        if A[i][i] == 0 :
            for j in range (i, len(A)) :
                if A[j][i] != 0 :
                    pVektor[i] = j
                    A[i], A[j] = A[j], A[i]
                    break
        else :
            pVektor[i] = i 
            #Ende vertauschen
        #quotienten ermitteln und eintragen
        for j in range (i+1, len(A)) :
            A[j][i] /= A[i][i]
            for k in range (i+1, len(A)) :
                A[j][k]-=A[j][i]*A[i][k] 
    return {"LU":A,"p":pVektor}

def swap(list,indexA,indexB):
    temp = list[indexA]
    list[indexA]= list[indexB]
    list[indexB] = temp

def permutation(p,x):
    y = x + []
    for i in range(0,len(p)):
        swap(y,p[i],i)
    return y


def vorwaerts(LU,x):
    w = int(math.sqrt(len(LU)))
    y = []
    for i in range(0,len(LU),w+1):
        y_temp = x[i%w]
        temp = 0
        for j in range(0,i%w):
            temp += y[j] * LU[int(i/w)*w+j]
        y_temp -= temp
        y.append(y_temp)
    return y

def rueckwaerts(LU,x):
    w = int(math.sqrt(len(LU)))
    y = []
    for i in range(len(LU)-1,-1,-w-1):
        y_temp = x[i%w]
        temp = 0
        for j in range(i%w,w-1):
            temp += y[w-2-j] * LU[int(i/w)*w+j+1]
        y_temp -= temp
        y.append(y_temp/LU[i])
    return list(reversed(y))

def get_A(n):
    A = []
    for i in range(0,n):
            zeile = []
            for j in range(0,n):
                if len(zeile) == i:
                    zeile.append(1)
                else:
                    zeile.append(0)
            if i == 0:
                zeile[i-1] = beta
            else:
                zeile[i-1] = -1*beta
            if i == n-1:
                zeile[-1] = 0
            A.append(zeile)
    return A

def get_b(n,beta):
    b = [1+beta]
    for i in range(2,n):
        b.append(1-beta)
    b.append(-1*beta)
    return b

if __name__ == "__main__":
    narray = [10,15,20]
    beta = 10
    for n in narray:
        A = get_A(n)
        b = get_b(n,beta)
        zerlegt = zerlegung(A)
        LU = []
        for array in zerlegt['LU']:
            LU += array
        p = zerlegt['p']
        y = vorwaerts(LU,permutation(p,b))
        print('Mit Pivot:')
        print(rueckwaerts(LU,y))
    for n in narray:
        A = get_A(n)
        b = get_b(n,beta)
        zerlegt = zerlegung_ohne_pivot(A)
        LU = []
        for array in zerlegt['LU']:
            LU += array
        p = zerlegt['p']
        y = vorwaerts(LU,permutation(p,b))
        print('Ohne Pivot:')
        print(rueckwaerts(LU,y))