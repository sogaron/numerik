import math
import numpy as np
import numpy.linalg as linalg
def zerlegung(A):
    pVektor= [0]* len(A)
    for i in range (len(A)) :
        #vertauschen
        biggest = A[i][i]
        index = i
        for j in range(i,len(A)):
            if abs(A[j][i]) > biggest:
                biggest = abs(A[j][i])
                index = j
        pVektor[i] = index
        A[index], A[i] = A[i], A[index]
        #quotienten ermitteln und eintragen
        for j in range (i+1, len(A)) :
            A[j][i] /= A[i][i]
            for k in range (i+1, len(A)) :
                A[j][k]-=A[j][i]*A[i][k] 
    return {"LU":A,"p":pVektor}

def swap(list,indexA,indexB):
    temp = list[indexA]
    list[indexA]= list[indexB]
    list[indexB] = temp

def permutation(p,x):
    y = x + []
    for i in range(0,len(p)):
        swap(y,p[i],i)
    return y


def vorwaerts(LU,x):
    w = int(math.sqrt(len(LU)))
    y = []
    for i in range(0,len(LU),w+1):
        y_temp = x[i%w]
        temp = 0
        for j in range(0,i%w):
            temp += y[j] * LU[int(i/w)*w+j]
        y_temp -= temp
        y.append(y_temp)
    return y

def rueckwaerts(LU,x):
    w = int(math.sqrt(len(LU)))
    y = []
    for i in range(len(LU)-1,-1,-w-1):
        y_temp = x[i%w]
        temp = 0
        for j in range(i%w,w-1):
            temp += y[w-2-j] * LU[int(i/w)*w+j+1]
        y_temp -= temp
        y.append(y_temp/LU[i])
    return list(reversed(y))

def get_eps(A,x,b):
    r_abs = np.absolute(np.subtract(b,A.dot(x)))
    s = np.add(np.absolute(A).dot(np.absolute(x)),np.absolute(b))
    epsilon = None
    for i in range(0,len(s)):
        if (epsilon == None or r_abs[i] / s[i] > epsilon):
            epsilon = r_abs[i] / s[i]
    return epsilon
        
if __name__ == "__main__":
    deltaarray = [10**-8,10**-10,10**-12]
    for delta in deltaarray:
        A = np.array([[3,2,1],[2,2*delta,2*delta],[1,2*delta,-1*delta]])
        x = np.array([1,2,3])
        b = np.array([3+3*delta,6*delta,2*delta])
        epsilon = get_eps(A,x,b)
        zerlegt = zerlegung(A.tolist())
        LU = []
        for array in zerlegt['LU']:
            LU += array
        p = zerlegt['p']
        y = vorwaerts(LU,permutation(p,b.tolist()))
        print(rueckwaerts(LU,y))
        print('prod',np.finfo(float).eps*linalg.cond(A))
        print('eps',epsilon)